import "../styles/modal.css";

import ej0 from '../public/assets/imgs/ejemplo-uso-0.jpg'
import ej1 from '../public/assets/imgs/ejemplo-uso-1.jpg'
import ej2 from '../public/assets/imgs/ejemplo-uso-2.jpg'

function toggleVisible() {
    const modal = document.querySelector(".modal");
    if (modal.classList.contains("visible")) {
        modal.classList.replace("visible", "oculto");
    } else {
        modal.classList.replace("oculto", "visible");
    }


}

export default function modal() {
    //esta bastante mal escrito este codigo. voy a cambiar de entorno y ahi agregar algo mas comodo de modificar
    // const container = document.querySelector(".container");

    const modal = document.createElement("DIV");
    const modalContainer = document.createElement("DIV");
    const header = document.createElement("DIV");

    modal.classList.add("modal", "visible");
    modalContainer.classList.add("modal-content");

    header.classList.add("modal-header");

    const headerContent = document.createElement("Div");
    const btnClose = document.createElement("I");
    btnClose.classList.add("btn-close");

    btnClose.addEventListener("click", toggleVisible)
    btnClose.innerHTML = "X";

    headerContent.appendChild(btnClose);

    headerContent.appendChild(btnClose);

    header.appendChild(headerContent);
    // modalContainer.innerText="HOLA";
    // modalContainer.innerHTML="HOLA";

    const linkRepo = document.createElement("A");
    linkRepo.href="https://gitlab.com/stfg.prof/hydra-webpack-mp"
    linkRepo.innerText= "\n\n Link al repositorio de esta aplicacion"
    linkRepo.setAttribute('target', '_blank');
    const descripcion = document.createElement("P");
    descripcion.innerText = "\n\n "+" Esta aplicacion esta basada en hydra-synth de Olivia Jack integrando el reconocimiento de manos utilizando mediapipe de google " + "\n" +  "\n";

    const p0 = document.createElement("P");
    p0.innerText = "\n\n al entrar en la web se pediran permisos para la camara una vez hecho esto, la camara capturarra la posicion de las manos como se ve a continuacion (Es importante que la camara caputure solamente una mano)" + "\n" +  "\n";

    const p1 = document.createElement("P");
    p1.innerText = " \n\n para agregar una operacion se utiliza el gesto de cerrar (el tiempo que se mantenga cerrado es importante) \n    se sabra que la aplicacion esta leyendo correctamente el gesto por que se vuelven blancas los puntos de trackeo."+ "\n" +  "\n";

    const p2 = document.createElement("P");
    p2.innerText =  " \n\n al entrar en la web se pediran permisos para la camara una vez hecho esto, la camara capturarra la posicion de las manos como se ve a continuacion "+ "\n" +  "\n";
    const space = document.createElement("P");
    space.innerText =  "\n\n";
    
    const img0 = document.createElement("IMG");
    // img.setAttribute("src","ejemplo-uso-0.jpg");//"../assets/imgs/ejemplo-uso-0.jpg");
    img0.src = ej0;
    img0.setAttribute("alt", "ejemplo 1 ");
    const img1 = document.createElement("img");
    // img.setAttribute("src","ejemplo-uso-0.jpg");//"../assets/imgs/ejemplo-uso-0.jpg");
    img1.src = ej1;
    img1.setAttribute("alt", "ejemplo 1 ");
    const img2 = document.createElement("img");
    // img.setAttribute("src","ejemplo-uso-0.jpg");//"../assets/imgs/ejemplo-uso-0.jpg");
    img2.src = ej2;
    img2.setAttribute("alt", "ejemplo 1 ");


    modalContainer.appendChild(header);

    modalContainer.appendChild(linkRepo);
    modalContainer.appendChild(descripcion);
    modalContainer.appendChild(p0);
    modalContainer.appendChild(img0);
    modalContainer.appendChild(p1);
    modalContainer.appendChild(img1);
    modalContainer.appendChild(p2);
    modalContainer.appendChild(img2);
    modalContainer.appendChild(space);

    modal.appendChild(modalContainer);;

    // container
    document.body.appendChild(modal);



}

// .modal-content {
//   background-color: #fefefe;
//   margin: auto;
//   padding: 20px;
//   border: 1px solid #888;
//   width: 80%;
// }