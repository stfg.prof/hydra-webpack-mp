import { calcularHydra, guardarDatoHydra } from './manejoDeHydra';
import { getFormaBasicaDeMano, getOrientacionDeMano, getcentroideMano, getUbicacionEnCuadrante } from './analisisManos';
import { drawLandmarks } from '@mediapipe/drawing_utils';
import hydraCanvas from '../components/hydra';


//implementar una marca de tiempo en vez de un contador de ciclos? asi poder medir los valores en segundos?

//basicamente podria hacerlo con un retaro el get elementeByClass pero bueno esto es lo primero q logre
export  function initProceso() {
  const container=document.querySelector(".container");
  const canvasElement = document.createElement("CANVAS");
  canvasElement.className = "output_canvas";
  canvasElement.width = innerWidth;// 1280 * .75;
  canvasElement.height = innerHeight;//720 * .75;
  canvasElement.id = "canvas-hydra"

  container.appendChild(canvasElement);
  container.appendChild(hydraCanvas());
  
    // calcularHydra(operacionesHydra);
};

const MINIMO_MANO_CERRADA = 5;
  const limiteContadorContinuidad = 10;//quizas este al dope
  var contadorContinuidad = 0;//quizas este al dope
  var manoCerrada = 0;
  var flagAplicarOperador = false;
  var continuidadMano = false, cambioDeEstado = false, estadoActual = false, EstadoAnterior = false;
  var operacionesHydra = [{
    "operacion": "osc",
    "args": [4, 0.5, 0.75]
  }]
var iniciado=true;
export function procesarResultados(results) {

  const canvasElement=document.querySelector("#canvas-hydra")
  const ctx = canvasElement.getContext('2d');
  const width = ctx.canvas.width;
  const height = ctx.canvas.height;
  if(iniciado){calcularHydra(operacionesHydra)
    iniciado=false;}
  //cuando trato de pasarle mas parametros se hace inocuo el gesto sobre hydra
  //DIBUJO CANVAS
  // ctx.drawImage(results.image, 0, 0, canvasElement.width, canvasElement.height);
  var multiHandLands = results.multiHandLandmarks;


  if (multiHandLands.length > 0) {
    ctx.save();
    ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);
    ctx.scale(-1, 1);
    ctx.translate(-canvasElement.width, 0);
    // console.log(multiHandLands);

    //DIBUJO LANDMARKS

    for (const landmarks of multiHandLands) {
      // drawConnectors(ctx, landmarks, HAND_CONNECTIONS,
      //   { color: '#00FF00', lineWidth: 5 });
      //   if(flagAplicarOperador)drawLandmarks(ctx, landmarks, { color: '#0000FF', lineWidth: .1 });
      //  else drawLandmarks(ctx, landmarks, { color: '#F005f0', lineWidth: .1 });
      if (flagAplicarOperador) drawLandmarks(ctx, landmarks, { color: '#FFFFFF', lineWidth: .1 });
      else drawLandmarks(ctx, landmarks, { color: '#000000', lineWidth: .1 });
    }
    //este for se repite con el siguiente, en el fondo no se si puedo combinarlos ya que implicaria un reset y transform por bucle, relentizando el dibujo
    ctx.resetTransform()
    ctx.strokeStyle = "#FFFFFF";
    for (let i = 0; i <= 3; i++) {
      ctx.beginPath();
      ctx.moveTo(width * (i / 3), 0);
      ctx.lineTo(width * (i / 3), height);
      ctx.stroke();
    }
    for (let i = 0; i <= 3; i++) {
      ctx.beginPath();
      ctx.moveTo(0, height * (i / 3));
      ctx.lineTo(width, height * (i / 3));
      ctx.stroke();
    }
    // for (const landmarks of multiHandLands) {
    //   for (const landmark of landmarks) {
    //     // if (landmark.visibility != null && landmark.visibility > .75) {
    //     // }
    //     let text = (1. - landmark.x).toPrecision(3).toString() + " , " + landmark.y.toPrecision(3).toString() + " , " + landmark.z.toPrecision(3).toString();
    //     ctx.fillText(text, (1. - landmark.x) * canvasElement.width, landmark.y * canvasElement.height);
    //     //ctx.fillText(text,(1.-landmark.x)*width,landmark.y*height);
    //   }
    // }
    ctx.restore();

    //ANALISIS MANOS

    if (multiHandLands.length == 1) {
      const landmarks = multiHandLands[0];
      const forma = getFormaBasicaDeMano(landmarks);
      // const orientacion = getOrientacionDeMano(landmarks);
      const centroide = getcentroideMano(landmarks);
      const cuadrante = getUbicacionEnCuadrante({ x: 1. - centroide.x, y: centroide.y, });
      // console.log("cuadrante")
      // console.log(cuadrante)
      if (forma == -1) {
        //console.log("Forma indefinida")
        //  flagAplicarOperador = false; //quizass es medio complejo pasar de cerrada para abierta de golpe
      } else if (forma == 1) {
        // console.log("Forma Cerrada")
        if (manoCerrada > MINIMO_MANO_CERRADA) flagAplicarOperador = true;
        if (continuidadMano) manoCerrada++;//quizas este al dope
      } else if (forma == 0) {
        // console.log("Forma Abierta")
        if (flagAplicarOperador) {
          switch (cuadrante) {//esto tendria que convertirlo en un dispatch mar oganizado? y el array en otro scope?
            case 1: guardarDatoHydra(operacionesHydra, ".rotate", [0, manoCerrada / 100.]);
              break;
            case 2: guardarDatoHydra(operacionesHydra, ".blend", ["src(o0)", ((manoCerrada) % 100) / 100.]);
              break;
            case 3: guardarDatoHydra(operacionesHydra, ".scrollX", [0, manoCerrada / 20.]);
              break;
            case 4: guardarDatoHydra(operacionesHydra, ".kaleid", manoCerrada / 20.);
              break;
            case 5: guardarDatoHydra(operacionesHydra, ".colorama", [manoCerrada / 20.]);
              break;
            case 6: guardarDatoHydra(operacionesHydra, ".mult", "noise(" + (manoCerrada / 20.).toString() + ")");
              break;
            case 7: guardarDatoHydra(operacionesHydra, ".diff", "osc(" + (manoCerrada / 10.).toString() + ",0.1,5).rotate(0,.1)");
              break;
            case 8: guardarDatoHydra(operacionesHydra, ".modulateHue", ["src(o0)", manoCerrada]);
              break;
            case 9: guardarDatoHydra(operacionesHydra, ".saturate", "() => Math.sin(time*0.5)*0.5+0.75");
              break;
            default:
              break;
          }
          //pensar como poder anidar facilmente operaciones
          const cadena = calcularHydra(operacionesHydra);
          console.log(cadena);
          flagAplicarOperador = false; //esto cambiaria algo si lo coloro junto a mano cerrada?
        }
        //aca iria aplicar transformacion
        manoCerrada = 0;
      }
      // if (orientacion) {
      //   console.log("de Frente o Dorso")
      // } else {
      //   console.log("de Costado")
      // }
      continuidadMano = true;
      contadorContinuidad = 0;
    } else {
      contadorContinuidad++;
      if (contadorContinuidad >= limiteContadorContinuidad) {
        continuidadMano = false;
      }
    }
  }
  else ctx.clearRect(0, 0, canvasElement.width, canvasElement.height);

}
