
export function isVisible(point) {
    if (point.visibility != null) {
        return (point.visibility > .75);
    } else return false;
}
export function jointIsVisible(firstPoint, midPoint, lastPoint) {
    //    if(firstPoint.visibility!=null  &&midPoint.visibility!=null  && lastPoint.visibility!=null) {
    // 	return (firstPoint.visibility >.75 &&midPoint.visibility>.75  && lastPoint.visibility>.75 );
    //     }else 
    return isVisible(firstPoint) && isVisible(midPoint) && isVisible(lastPoint);
}

//#esta compensado que no esten en las coord [0,0] tengo que evaluar mejor esta posibilidad
export function getAngle(firstPoint, midPoint, lastPoint) {

    var result = Math.atan2(lastPoint.y - midPoint.y, lastPoint.x - midPoint.x) - Math.atan2(firstPoint.y - midPoint.y, firstPoint.x - midPoint.x);
    result = Math.abs(result); // Angle should never be negative
    if (result > Math.PI) result = 2. * Math.PI - result; // Always get the acute representation of the angle
    return result;
}
// def getAngleSigned(firstPoint, midPoint, lastPoint):
//   return np.arctan2(lastPoint[1] - midPoint[1],lastPoint[0] - midPoint[0]) - np.arctan2(firstPoint[1] - midPoint[1],firstPoint[0] - midPoint[0])
export function getAnglDegree(firstPoint, midPoint, lastPoint) {
    return getAngle(firstPoint, midPoint, lastPoint) * 180.0 / Math.PI;
}


export function distance(x1, y1, x2, y2) {
    return Math.hypot(x2 - x1, y2 - y1)
}

export function cercania(a, b, k = 0.08) {//75. / innerHeight este metodo no tiene en cuenta la distancia a la camara
    return (distance(a.x, a.y, b.x, b.y) <= k)&&a.z<0.&& b.z<0.0;
}
/*export function distance(x1, y1,z1, x2, y2,z2) {
    return Math.hypot(x2 - x1, y2 - y1,z2- z1)
}

export function cercania(a, b, k = 0.08) {//75. / innerHeight este metodo no tiene en cuenta la distancia a la camara
    return (distance(a.x, a.y,a.z, b.x, b.y,b.z) <= k);
}
*/