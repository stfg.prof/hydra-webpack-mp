import { cercania, distance, getAnglDegree, getAngle } from './mates';

export function getUbicacionEnCuadrante(point) {
    const x = point.x, y = point.y;
    //en terminos de optimizacin el return en cada if seria lo presiso?
    //hay alguna operacion mas simple? o anidando?
    var cuadrante = undefined;
    if (x > 1. || y > 1. || x < 0. || y < 0.) {
        cuadrante = -1;
    } else if ((x <= 1. && y < 1.) && (x >= .6666 && y >= 0.6666)) {
        cuadrante = 9;

    } else if ((x < .6666 && y < 1.) && (x >= .3333 && y >= 0.6666)) {
        cuadrante = 8;
    } else if ((x < .3333 && y < 1.) && (y >= 0.6666)) {
        cuadrante = 7;
    } else if ((x <= 1. && y < .6666) && (x >= .6666 && y >= .3333)) {
        cuadrante = 6;

    } else if ((x <= .6666 && y < .6666) && (x >= .3333 && y >= .3333)) {
        cuadrante = 5;

    } else if ((x < .3333 && y < .6666) && y >= .3333) {
        cuadrante = 4;

    } else if (x <= 1. && y < .3333 && x >= .6666){
        cuadrante = 3;

    } else if (x < .6666 && x >= .3333 ) {
        cuadrante = 2;

    } else {
        cuadrante = 1;
    }
    return cuadrante
}
export function getcentroideMano(landmarks) {
    const len = 21; //algo se cambio en js que rompo los for in con un offset  y un fast
    var posX = 0, posY = 0, posZ = 0;

    for (var i = 0; i < len; i++) {
        const landmark = landmarks[i];


        posX += landmark.x;
        posY += landmark.y;
        posZ += landmark.z;
    }
    posX /= len;
    posY /= len;
    posZ /= len;
    return { x: posX, y: posY, z: posZ }
}
export function getFormaBasicaDeMano(landmarks, limiteAnguloCerrado = 55., limiteAnguloAbierto = 145.) {

    var anguloindice = getAnglDegree(landmarks[8], landmarks[5], landmarks[1])
    var anguloMayor = getAnglDegree(landmarks[12], landmarks[9], landmarks[1])
    var anguloAnular = getAnglDegree(landmarks[16], landmarks[13], landmarks[1])
    var anguloMenor = getAnglDegree(landmarks[20], landmarks[17], landmarks[1])

    if (anguloindice < limiteAnguloCerrado && anguloMayor < limiteAnguloCerrado && anguloAnular < limiteAnguloCerrado && anguloMenor < limiteAnguloCerrado) {
        return 1;
    }
    else if (anguloindice > limiteAnguloAbierto && anguloMayor > limiteAnguloAbierto && anguloAnular > limiteAnguloAbierto && anguloMenor > limiteAnguloAbierto) {
        return 0;
    } else {
        return -1;
    }

}
export function getOrientacionDeMano(landmarks, distanciaMin = 0.095) {//orientacion()
    //con estos metodos estoy tomando valores relativos sin contemplar la distancia de la camara (para ello podria en un futuro capturar el tamaño total y relacionarlo con el largo-ancho de la imagen-caprturaa) 
    var relacionAB = distance(landmarks[5].x, landmarks[5].y, landmarks[17].x, landmarks[17].y);
    var relacionAD = distance(landmarks[5].x, landmarks[5].y, landmarks[1].x, landmarks[1].y);
    var relacionBC = distance(landmarks[17].x, landmarks[17].y, landmarks[0].x, landmarks[0].y);
    //esto lo puedo re hacer segun cercania? 
    return relacionAB > distanciaMin && relacionAD > distanciaMin && relacionBC > distanciaMin;
}
