
const LIMITE_OPERACIONES=9;

export function ensamblar(operacion, args) {
  //https://stackoverflow.com/questions/11182924/how-to-check-if-javascript-object-is-json
  //quizas es muy mala practica pero si operacion es un array
  //ir recorriendo json internos ?? como poner los arg de cierre??
  if (args.constructor == Object) {
    //agregar una clausula de recorrer arrays dentro del obj?
    return operacion + "(" + ensamblar(args["operacion"], args["args"]) + ")";
  } else {
    return operacion + "(" + args.toString() + ")"
  };
}

export function crearDatoHydra(operacion, args) {
  return {
    "operacion": operacion,
    "args": args
  };
}

export function guardarDatoHydra(arreglo, operacion, args=[]) {
  if(arreglo.length>LIMITE_OPERACIONES) arreglo.splice(1, 1);
  arreglo.push(crearDatoHydra(operacion, args));
}

export function calcularHydra(listaOperaciones){

  var str = ""

  for (var i = 0; i < listaOperaciones.length; i++) {
    var temp = listaOperaciones[i];
    str += ensamblar(temp["operacion"], temp["args"]);
  }
  str += ".out()";

  eval(str); //esto obviamente siempre es una re brecha 
  return str;
}