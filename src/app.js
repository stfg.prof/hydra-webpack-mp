
import hydraCanvas from './components/hydra';
import "./styles/style.css";
import modal from './components/modal';
// import  'hydra-synth';
//parche hardcodeado
//en un futuro armar un fork con un build tipico de lib

import { Camera } from '@mediapipe/camera_utils';
import "@mediapipe/control_utils";
import { Hands} from '@mediapipe/hands/hands'

import {procesarResultados,initProceso} from './utils/procesarResultados';

//algo no me convence de tener separados los elementos 
//o agregarlos al final, tengo que resolver una jerarquia de elemento compartido a individuales
//y desacoplar el anailis del dibujo
const container = document.createElement("div");
container.classList.add("container");
document.body.appendChild(container);
//agregue una funcion init para previamente los elementos al body
initProceso(); //me gustaria ya tenerlos declarados pero no se como configure webpack que se me pisa el html
//hago esto para poder agregar un modal dentro de un container

modal();


const videoElement = document.createElement("VIDEO");
videoElement.className = "input_video";
videoElement.style.display = "none";
// container.appendChild(videoElement);
document.body.appendChild(videoElement);
const hands = new Hands({locateFile: (file) => `public/hands/${file}`});
// const hands = new Hands({
//   locateFile: (file) => {
//     return `https://cdn.jsdelivr.net/npm/@mediapipe/hands/${file}`;
//   }
// });
hands.setOptions({
  maxNumHands: 2,
  modelComplexity: 1,
  minDetectionConfidence: 0.5,
  minTrackingConfidence: 0.5
});
hands.onResults(procesarResultados);

const camera = new Camera(videoElement, {
  onFrame: async () => {
    await hands.send({ image: videoElement });
  },
  width: 1280,
  height: 720
});
camera.start();


